package com.example.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import io.micronaut.core.annotation.Introspected;
import lombok.*;
import software.amazon.awssdk.enhanced.dynamodb.extensions.annotations.DynamoDbVersionAttribute;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbSortKey;

/**
 * Represents the aggregated transaction data
 */
@DynamoDbBean
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Introspected
public class AggregatedTransactionData {

    private String partitionKey;
    private String sortKey;
    private String merchant;
    private int count;
    private Long version;

    @DynamoDbPartitionKey
    public String getPartitionKey() {
        return partitionKey;
    }

    @DynamoDbSortKey
    public String getSortKey() {
        return sortKey;
    }

    @DynamoDbVersionAttribute
    public Long getVersion() {
        return version;
    }


}
