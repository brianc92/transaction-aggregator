package com.example.exception;

public class LockedRowException extends RuntimeException{

    public LockedRowException() {}

    public LockedRowException(final String message) {
        super(message);
    }
}
