package com.example.lambda;

import com.amazonaws.services.dynamodbv2.document.ItemUtils;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.lambda.runtime.events.KinesisEvent;
import com.amazonaws.services.lambda.runtime.events.KinesisEvent.KinesisEventRecord;
import com.example.aggreagation.Aggregator;
import com.example.model.OriginTransactionData;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.function.aws.MicronautRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Map;

@Introspected
public class NewUserTransactionConsumerService extends MicronautRequestHandler<KinesisEvent, Void> {

    private static final String INSERT = "INSERT";

    @Inject
    private Aggregator aggregator;

    private static final Logger LOGGER = LoggerFactory.getLogger(NewUserTransactionConsumerService.class);

    @Override
    public Void execute(KinesisEvent event) {

        LOGGER.debug("Lambda Consumer called with request body 'KinesisEvent.getRecords()' = {}", event.getRecords());

        for (KinesisEventRecord rec : event.getRecords()) {
            try {

                var objectMapper = new ObjectMapper();
                objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

                var byteBuffer = rec.getKinesis().getData();
                var rootNode = objectMapper.readTree(byteBuffer.array());
                var eventField = rootNode.get("eventName");
                var eventName = eventField.asText();
                LOGGER.debug(" EVENT = {}", eventName);

                if (INSERT.equals(eventName)) {
                    LOGGER.info("Received new Dynambo DB insert event");
                    var dynamodbRecord = rootNode.get("dynamodb");
                    var newImage = dynamodbRecord.get("NewImage");
                    LOGGER.debug("NEW IMAGE = {}", newImage);
                    Map<String, AttributeValue> itemMap = objectMapper.convertValue(newImage, new TypeReference<>(){});
                    var item = ItemUtils.toItem(itemMap);
                    LOGGER.debug("ITEM = {} ", item);
                    OriginTransactionData newTransaction = objectMapper.readValue(item.toJSON(), OriginTransactionData.class);
                    aggregator.aggregateTransaction(newTransaction);
                }

            } catch (IOException e) {
                LOGGER.error("Unable to create JSON from kinesis record", e);
            }
        }
        return null;
    }
}


