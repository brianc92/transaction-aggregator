package com.example.dbclient;

import com.example.exception.LockedRowException;
import com.example.model.AggregatedTransactionData;

import java.util.Optional;

public interface IDBClientProvider {

    Optional<AggregatedTransactionData> getAggregatedItemFor(final String partitionKey, final String sortKey);

    void putOrUpdateAggregatedItem(final AggregatedTransactionData aggregatedTransactionData) throws LockedRowException;

}
