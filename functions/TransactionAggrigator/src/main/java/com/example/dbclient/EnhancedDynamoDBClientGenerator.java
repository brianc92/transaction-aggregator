package com.example.dbclient;

import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;

import javax.inject.Singleton;

//Wrapper class to allow easier mocking of the enhanced dynamo db client
@Singleton
public class EnhancedDynamoDBClientGenerator {

    public DynamoDbEnhancedClient generateEnhancedClient() {
        return DynamoDbEnhancedClient.create();
    }
}


