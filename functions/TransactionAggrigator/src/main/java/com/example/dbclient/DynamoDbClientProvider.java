package com.example.dbclient;

import com.example.aggreagation.Aggregator;
import com.example.exception.LockedRowException;
import com.example.model.AggregatedTransactionData;
import com.example.model.OriginTransactionData;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;
import software.amazon.awssdk.enhanced.dynamodb.model.PageIterable;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.ConditionalCheckFailedException;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
@Getter
public class DynamoDbClientProvider implements IDBClientProvider {

  public static final String AGG_TXN_TABLE_NAME = "agg-user-transactions";

  private final DynamoDbEnhancedClient enhancedClient;

  private static final Logger LOGGER = LoggerFactory.getLogger(DynamoDbClientProvider.class);

  @Inject
  public DynamoDbClientProvider(EnhancedDynamoDBClientGenerator enhancedDynamoDBClientGenerator) {
    enhancedClient = enhancedDynamoDBClientGenerator.generateEnhancedClient();
  }

  private DynamoDbTable<AggregatedTransactionData> getAggTransactionTable() {
    return enhancedClient.table(AGG_TXN_TABLE_NAME, TableSchema.fromBean(AggregatedTransactionData.class));
  }

  @Override
  public Optional<AggregatedTransactionData> getAggregatedItemFor(final String partitionKey, final String sortKey) {

    LOGGER.debug("Get Agg item request for keys {} {} ", partitionKey, sortKey);
    DynamoDbTable<AggregatedTransactionData> aggTable = getAggTransactionTable();
    Key key = Key.builder().
            partitionValue(partitionKey).
            sortValue(sortKey).build();

    return Optional.ofNullable(aggTable.getItem(key));
  }

  @Override
  public void putOrUpdateAggregatedItem(final AggregatedTransactionData aggregatedTransactionData) throws LockedRowException {
    LOGGER.debug("DB PUT or UPDATE aggregation request for {} ", aggregatedTransactionData);
    DynamoDbTable<AggregatedTransactionData> aggTable = getAggTransactionTable();

    //Chance that the item row is locked by another process if so throw run time exception
    try {
      aggTable.putItem(aggregatedTransactionData);
    } catch (ConditionalCheckFailedException e) {
      throw new LockedRowException("Item locked for partition " + aggregatedTransactionData.getPartitionKey() + "and sort key " + aggregatedTransactionData.getSortKey());
    }
  }

}
