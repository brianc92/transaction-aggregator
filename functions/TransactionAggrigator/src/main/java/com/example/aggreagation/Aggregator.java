package com.example.aggreagation;

import com.example.dbclient.IDBClientProvider;
import com.example.exception.LockedRowException;
import com.example.model.AggregatedTransactionData;
import com.example.model.OriginTransactionData;
import com.example.util.ITransactionConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Optional;

/**
 * Orchestrates the aggregation of incoming transactions
 */
@Singleton
public class Aggregator {

    private final IDBClientProvider dbClientProvider;
    private final ITransactionConverter transactionConverter;

    private static final Logger LOGGER = LoggerFactory.getLogger(Aggregator.class);

    @Inject
    public Aggregator(final IDBClientProvider dbClientProvider, final ITransactionConverter transactionConverter) {
        this.dbClientProvider = dbClientProvider;
        this.transactionConverter = transactionConverter;
    }

    /**
     * Orchestrates the aggregation process.
     *
     * will retry for a set number of times, if the update is unsuccessful
     * due the the row item being locked by another process
     *
     * @param originTransaction contains original data
     */
    public void aggregateTransaction(final OriginTransactionData originTransaction) {

        LOGGER.info("Aggregating new transaction {}", originTransaction);
        var start = System.currentTimeMillis();

        //TODO do we want this as a property variable?  also do we want an exception thrown here?
        //This is here to stop an infinite loop potentially happening
        for (int i = 0 ; i < 3; i++) {
            try {
                aggregateAndUpdateTable(originTransaction);
                LOGGER.info("Aggregation complete in {}ms for {}", System.currentTimeMillis() - start, originTransaction);
                return;
            } catch (LockedRowException e) {
                LOGGER.warn(e.getMessage());
            }
        }
        LOGGER.error("All retries failed for user {} merchant {}", originTransaction.getUserId(), originTransaction.getMerchant());
    }


    /**
     * converts the incoming original data into the aggregation format
     *  checks the aggregation table to see if a row for this item exists
     *  if it does, increments the count and writes back
     *  if it does not, puts a new item with a count of 1
     *
     * @param originTransaction new agg data
     * @throws LockedRowException if row has been updated by another process
     */
    private void aggregateAndUpdateTable(final OriginTransactionData originTransaction) throws LockedRowException {

        AggregatedTransactionData newAggData = transactionConverter.convertOriginalToAggregated(originTransaction);

        AggregatedTransactionData dataToStore;

        Optional<AggregatedTransactionData> existingAggData = dbClientProvider.getAggregatedItemFor(newAggData.getPartitionKey(), newAggData.getSortKey());

        if (existingAggData.isPresent()) {
            LOGGER.info("Item does exists for {} {} performing aggregations", newAggData.getPartitionKey(),  newAggData.getSortKey());
            LOGGER.debug("existing = {}", existingAggData);
            dataToStore = performAggregations(newAggData, existingAggData.get());
        } else {
            LOGGER.info("NO item exists for {} {} adding new item", newAggData.getPartitionKey(), newAggData.getSortKey());
            dataToStore = initialiseNewItem(newAggData);
        }

        dbClientProvider.putOrUpdateAggregatedItem(dataToStore);
    }

    private AggregatedTransactionData performAggregations(AggregatedTransactionData newAggData, final AggregatedTransactionData existingAggData) {
        incrementCount(existingAggData);
        return existingAggData;
    }

    private void incrementCount(AggregatedTransactionData existingAggData) {
        int newCount = existingAggData.getCount() + 1;
        existingAggData.setCount(newCount);
    }

    private AggregatedTransactionData initialiseNewItem(AggregatedTransactionData newAggData) {
        initialiseCount(newAggData);
        return newAggData;
    }

    private void initialiseCount(AggregatedTransactionData newAggData) {
        newAggData.setCount(1);
    }
}
