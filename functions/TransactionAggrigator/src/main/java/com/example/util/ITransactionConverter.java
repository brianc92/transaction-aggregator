package com.example.util;

import com.example.model.AggregatedTransactionData;
import com.example.model.OriginTransactionData;

public interface ITransactionConverter {

    AggregatedTransactionData convertOriginalToAggregated(final OriginTransactionData originTransactionData);
}
