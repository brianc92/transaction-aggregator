package com.example.util;

import com.example.model.AggregatedTransactionData;
import com.example.model.OriginTransactionData;

import javax.inject.Singleton;

/**
 * Converts an incoming Original transaction data into the format
 * that will be stored in the aggregation table
 *
 * No count value is set at this stage
 */
@Singleton
public class TransactionConverter implements ITransactionConverter{

    private static final String SEPARATOR = "#";
    private static final String PREFIX = "agg";
    private static final Integer MAX_PARTITION_SIZE = 200;


    @Override
    public AggregatedTransactionData convertOriginalToAggregated(final OriginTransactionData originTransactionData) {
        var aggregatedTransactionData = new AggregatedTransactionData();
        aggregatedTransactionData.setPartitionKey(generatePartitionKey(originTransactionData));
        aggregatedTransactionData.setSortKey(generateSortKey(originTransactionData));
        aggregatedTransactionData.setMerchant(originTransactionData.getMerchant());

        return aggregatedTransactionData;
    }

    /**
     * Generate a partition key in format userId#partition num
     * partition number is generate by hashing the merchant String value, so each merchant will always
     * generate the same partition number.
     * @param originTransactionData contains data needed
     * @return generated partition key
     */
    private String generatePartitionKey(final OriginTransactionData originTransactionData) {
        int partitionNum = originTransactionData.getMerchant().hashCode() % MAX_PARTITION_SIZE + 1;
        return originTransactionData.getUserId() + SEPARATOR + partitionNum;
    }

    /**
     * Generate sort key in the format agg#merchant#year#month
     * @param originTransactionData contains data needed
     * @return generated sort key
     */
    private String generateSortKey(final OriginTransactionData originTransactionData) {
        return PREFIX + SEPARATOR +
                originTransactionData.getMerchant() + SEPARATOR +
                originTransactionData.getYear() + SEPARATOR +
                originTransactionData.getMonth();
    }
}
