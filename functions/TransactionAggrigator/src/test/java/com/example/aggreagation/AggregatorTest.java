package com.example.aggreagation;

import com.example.dbclient.IDBClientProvider;
import com.example.exception.LockedRowException;
import com.example.model.AggregatedTransactionData;
import com.example.model.OriginTransactionData;
import com.example.util.ITransactionConverter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.mockito.Mockito.*;

class AggregatorTest {

    private static final Integer USER_1 = 1;
    private static final String MERCHANT_1 = "MERCHANT_1";
    private static final Integer YEAR_1 = 2021;
    private static final Integer MONTH_1 = 4;

    private static final String PARTITION_KEY_1 = USER_1 + "#" + 179;
    private static final String SORT_KEY_1 = "agg#" + MERCHANT_1 + "#" + YEAR_1 + "#" + MONTH_1;

    @Mock
    private IDBClientProvider dbClientProvider;

    @Mock
    private ITransactionConverter transactionConverter;

    private static Aggregator aggregator;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        aggregator = new Aggregator(dbClientProvider, transactionConverter);
    }

    @Test
    void testAggregationNoExistingData() {

        OriginTransactionData originTransactionData = getDummyOriginTransaction();

        when(transactionConverter.convertOriginalToAggregated(originTransactionData)).thenReturn(getAggregatedDataWithCount(0));
        when(dbClientProvider.getAggregatedItemFor(PARTITION_KEY_1, SORT_KEY_1)).thenReturn(Optional.empty());

        aggregator.aggregateTransaction(originTransactionData);

        AggregatedTransactionData expectedAggregatedData = getAggregatedDataWithCount(1);
        verify(dbClientProvider, times(1)).putOrUpdateAggregatedItem(expectedAggregatedData);

    }

    @Test
    void testAggregationWithExistingData() {

        OriginTransactionData originTransactionData = getDummyOriginTransaction();

        when(transactionConverter.convertOriginalToAggregated(originTransactionData)).thenReturn(getAggregatedDataWithCount(0));
        when(dbClientProvider.getAggregatedItemFor(PARTITION_KEY_1, SORT_KEY_1)).thenReturn(Optional.of(getAggregatedDataWithCount(1)));

        aggregator.aggregateTransaction(originTransactionData);

        AggregatedTransactionData expectedAggregatedData = getAggregatedDataWithCount(2);
        verify(dbClientProvider, times(1)).putOrUpdateAggregatedItem(expectedAggregatedData);

    }

    /**
     * Simulate that on the first insert attempt, the row has been locked by another process, so a retry is performed
     * where the new value in the agg table is retrieved that was set by the other process, and aggregated successfully
     */
    @Test
    void testLockedRowSuccessfulRetry() {
        OriginTransactionData originTransactionData = getDummyOriginTransaction();

        //Ensure new object created each time the conversion made
        when(transactionConverter.convertOriginalToAggregated(originTransactionData))
                .thenReturn(getAggregatedDataWithCount(0))
                .thenReturn(getAggregatedDataWithCount(0));

        //On the retry, simulate the count has been updated by another process
        when(dbClientProvider.getAggregatedItemFor(PARTITION_KEY_1, SORT_KEY_1))
                .thenReturn(Optional.of(getAggregatedDataWithCount(1)))
                .thenReturn(Optional.of(getAggregatedDataWithCount(2)));

        //simulate low rocked exception on first attempt
        doThrow(LockedRowException.class)
                .doNothing()
                .when(dbClientProvider).putOrUpdateAggregatedItem(any(AggregatedTransactionData.class));

        aggregator.aggregateTransaction(originTransactionData);

        //Exception thrown on this attempt
        verify(dbClientProvider, times(1)).putOrUpdateAggregatedItem(getAggregatedDataWithCount(2));
        //This attempt successful
        verify(dbClientProvider, times(1)).putOrUpdateAggregatedItem(getAggregatedDataWithCount(3));
    }

    /**
     * Simulate that for all put request, the row has been locked by another process, so a retry is performed
     * up until the max limit (currently 3) then the process is stopped.
     */
    @Test
    void testLockedRowRetryLimitExceeded() {
        OriginTransactionData originTransactionData = getDummyOriginTransaction();

        //Ensure new object created each time the conversion made
        when(transactionConverter.convertOriginalToAggregated(originTransactionData))
                .thenReturn(getAggregatedDataWithCount(0))
                .thenReturn(getAggregatedDataWithCount(0))
                .thenReturn(getAggregatedDataWithCount(0))
                .thenReturn(getAggregatedDataWithCount(0));


        //On the retry, simulate the count has been updated by another process
        when(dbClientProvider.getAggregatedItemFor(PARTITION_KEY_1, SORT_KEY_1))
                .thenReturn(Optional.of(getAggregatedDataWithCount(1)))
                .thenReturn(Optional.of(getAggregatedDataWithCount(2)))
                .thenReturn(Optional.of(getAggregatedDataWithCount(3)))
                .thenReturn(Optional.of(getAggregatedDataWithCount(4)));

        //simulate low rocked exception on three attempts
        doThrow(LockedRowException.class)
        .doThrow(LockedRowException.class)
        .doThrow(LockedRowException.class)
                .when(dbClientProvider).putOrUpdateAggregatedItem(any(AggregatedTransactionData.class));

        aggregator.aggregateTransaction(originTransactionData);

        //3 attempts where exception thrown
        verify(dbClientProvider, times(1)).putOrUpdateAggregatedItem(getAggregatedDataWithCount(2));
        verify(dbClientProvider, times(1)).putOrUpdateAggregatedItem(getAggregatedDataWithCount(3));
        verify(dbClientProvider, times(1)).putOrUpdateAggregatedItem(getAggregatedDataWithCount(4));

    }

    private OriginTransactionData getDummyOriginTransaction() {
        OriginTransactionData originTransactionData = new OriginTransactionData();
        originTransactionData.setUserId(USER_1);
        originTransactionData.setMerchant(MERCHANT_1);
        originTransactionData.setMonth(MONTH_1);
        originTransactionData.setYear(YEAR_1);
        return originTransactionData;
    }

    private AggregatedTransactionData getAggregatedDataWithCount(final int count) {
        AggregatedTransactionData aggregatedTransactionData = new AggregatedTransactionData();
        aggregatedTransactionData.setPartitionKey(PARTITION_KEY_1);
        aggregatedTransactionData.setSortKey(SORT_KEY_1);
        aggregatedTransactionData.setMerchant(MERCHANT_1);
        aggregatedTransactionData.setCount(count);
        return aggregatedTransactionData;
    }
}