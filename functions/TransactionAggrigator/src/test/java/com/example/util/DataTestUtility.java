package com.example.util;

import com.example.aggreagation.Aggregator;
import com.example.model.OriginTransactionData;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;
import software.amazon.awssdk.enhanced.dynamodb.model.PageIterable;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Using for test purposes.  will take all items from the existing test transactions table,
 * and aggregate them into a new agg table
 */
@MicronautTest
public class DataTestUtility {

    public static final String TX_TABLE_NAME = "user-transactions";

    @Inject
    Aggregator aggregator;

    DynamoDbEnhancedClient enhancedClient;

    @BeforeEach
    void setUp() {
        enhancedClient = DynamoDbEnhancedClient.create();
    }


    /**
     * gets all the existing data already present in the user-transactions table and
     * runs aggregation on each one populating the aggregation table
     *
     * comment out @Disabled to run this (don't want this running on builds)
     */
    @Test
    @Disabled
    void aggregateAllExistingOriginData() {

        System.out.println("AGGREGATING EXISTING DATA.........");

        List<OriginTransactionData> originalData = getExistingData();

        for (OriginTransactionData originTransaction : originalData) {
            aggregator.aggregateTransaction(originTransaction);
        }

        System.out.println("FINISHED AGGREGATING DATA");
    }

    @Test
    @Disabled
    void insertNewOriginData() throws IOException {

        ClassLoader classLoader = getClass().getClassLoader();

        File file = new File(classLoader.getResource("newTransactions.json").getFile());

        final ObjectMapper objectMapper = new ObjectMapper();
        List<OriginTransactionData> transactionList = objectMapper.readValue(
                file,
                new TypeReference<>(){});

        transactionList.forEach(this::insertNewEntry);

    }

    private void insertNewEntry(OriginTransactionData originTransactionData) {
        System.out.println("Adding new transaction to origin table: " + originTransactionData);
        getTransactionTable().putItem(originTransactionData);
    }


    private List<OriginTransactionData> getExistingData() {

        DynamoDbTable<OriginTransactionData> originDataTable = getTransactionTable();
        PageIterable<OriginTransactionData> iter = originDataTable.scan();
        return iter.items().stream().collect(Collectors.toList());
    }

    private DynamoDbTable<OriginTransactionData> getTransactionTable() {
        return enhancedClient.table(TX_TABLE_NAME, TableSchema.fromBean(OriginTransactionData.class));
    }
}
