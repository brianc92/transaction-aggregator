package com.example.util;

import com.example.model.AggregatedTransactionData;
import com.example.model.OriginTransactionData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TransactionConverterTest {

    private static final Integer USER_1 = 1;
    private static final String MERCHANT_1 = "MERCHANT_1";
    private static final Integer YEAR_1 = 2021;
    private static final Integer MONTH_1 = 4;

    private static final String PARTITION_KEY_1 = USER_1 + "#" + 179;
    private static final String SORT_KEY_1 = "agg#" + MERCHANT_1 + "#" + YEAR_1 + "#" + MONTH_1;

    private static final Integer USER_2 = 2;
    private static final String MERCHANT_2 = "MERCHANT 2";
    private static final Integer YEAR_2 = 2020;
    private static final Integer MONTH_2 = 5;

    private static final String PARTITION_KEY_2 = USER_2 + "#" + 27;
    private static final String SORT_KEY_2 = "agg#" + MERCHANT_2 + "#" + YEAR_2 + "#" + MONTH_2;


    private static TransactionConverter converter;

    @BeforeAll
    public static void setUp() {
        converter = new TransactionConverter();
    }

    @Test
    void testConverter() {

        OriginTransactionData original = getDummyOriginData(USER_1, MERCHANT_1, YEAR_1, MONTH_1);
        AggregatedTransactionData aggregated = getExpectedAggregateData(PARTITION_KEY_1, SORT_KEY_1, MERCHANT_1);

        assertEquals(aggregated, converter.convertOriginalToAggregated(original));

    }

    @Test
    void testConverterWithWhitespaceInMerchant() {

        OriginTransactionData original = getDummyOriginData(USER_2, MERCHANT_2, YEAR_2, MONTH_2);
        AggregatedTransactionData aggregated = getExpectedAggregateData(PARTITION_KEY_2, SORT_KEY_2, MERCHANT_2);

        assertEquals(aggregated, converter.convertOriginalToAggregated(original));

    }

    public OriginTransactionData getDummyOriginData(Integer user, String merchant, Integer year, Integer month) {
        OriginTransactionData data = new OriginTransactionData();
        data.setUserId(user);
        data.setMerchant(merchant);
        data.setYear(year);
        data.setMonth(month);

        return data;
    }

    public AggregatedTransactionData getExpectedAggregateData(String partitionKey, String sortKey, String merchant) {
        AggregatedTransactionData aggregatedTransactionData = new AggregatedTransactionData();
        aggregatedTransactionData.setPartitionKey(partitionKey);
        aggregatedTransactionData.setSortKey(sortKey);
        aggregatedTransactionData.setMerchant(merchant);

        return aggregatedTransactionData;
    }



}