package com.example.dbclient;

import com.example.exception.LockedRowException;
import com.example.model.AggregatedTransactionData;
import com.example.model.OriginTransactionData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;
import software.amazon.awssdk.enhanced.dynamodb.model.PageIterable;
import software.amazon.awssdk.services.dynamodb.model.ConditionalCheckFailedException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class DynamoDbClientProviderTest {

    @Mock
    EnhancedDynamoDBClientGenerator enhancedDynamoDBClientGenerator;

    @Mock
    private DynamoDbEnhancedClient dynamoDbEnhancedClient;

    @Mock
    private DynamoDbTable<OriginTransactionData> originTable;

    @Mock
    private DynamoDbTable<AggregatedTransactionData> aggTable;

    @Mock
    PageIterable<OriginTransactionData> originResult;

    private DynamoDbClientProvider provider;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        when(enhancedDynamoDBClientGenerator.generateEnhancedClient()).thenReturn(dynamoDbEnhancedClient);

        when(dynamoDbEnhancedClient.table(eq("user-transactions"), any(TableSchema.class))).thenReturn(originTable);
        when(dynamoDbEnhancedClient.table(eq("agg-user-transactions"), any(TableSchema.class))).thenReturn(aggTable);

        provider = new DynamoDbClientProvider(enhancedDynamoDBClientGenerator);
    }

    @Test
    void testNoAggItem() {
        when(aggTable.getItem(any(Key.class))).thenReturn(null);

        assertEquals(Optional.empty(), provider.getAggregatedItemFor("1", "2"));
    }

    @Test
    void testAggItemFound() {
        AggregatedTransactionData aggregatedTransactionData = new AggregatedTransactionData();

        when(aggTable.getItem(any(Key.class))).thenReturn(aggregatedTransactionData);

        Optional<AggregatedTransactionData> result = provider.getAggregatedItemFor("1", "2");

        assertEquals(aggregatedTransactionData, result.get());
    }

    @Test
    void testAddAggItem() {
        AggregatedTransactionData aggregatedTransactionData = new AggregatedTransactionData();
        provider.putOrUpdateAggregatedItem(aggregatedTransactionData);

        verify(aggTable, times(1)).putItem(aggregatedTransactionData);
    }

    @Test
    void testAddAggItemRowLocked() {
        AggregatedTransactionData aggregatedTransactionData = new AggregatedTransactionData();

        doThrow(ConditionalCheckFailedException.class).when(aggTable).putItem(any(AggregatedTransactionData.class));

        Throwable exception = assertThrows(LockedRowException.class, () -> provider.putOrUpdateAggregatedItem(aggregatedTransactionData));

    }

}