package com.example.lambda;

import com.amazonaws.services.lambda.runtime.events.KinesisEvent;
import com.example.aggreagation.Aggregator;
import com.example.model.OriginTransactionData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

class NewUserTransactionConsumerServiceTest {


    @Mock
    private Aggregator aggregator;

    @Mock
    KinesisEvent kinesisEvent;

    @Mock
    KinesisEvent.Record kinesisRecord;

    @Mock
    KinesisEvent.KinesisEventRecord kinesisEventRecord;

    @InjectMocks
    private NewUserTransactionConsumerService service;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        when(kinesisEvent.getRecords()).thenReturn(Collections.singletonList(kinesisEventRecord));
        when(kinesisEventRecord.getKinesis()).thenReturn(kinesisRecord);
    }

    @Test
    void testRemoveEvent() {

        when(kinesisRecord.getData()).thenReturn(getDummyData("\"REMOVE\""));

        service.execute(kinesisEvent);

        verify(aggregator, times(0)).aggregateTransaction(any());
    }

    @Test
    void testModifyEvent() {

        when(kinesisRecord.getData()).thenReturn(getDummyData("\"MODIFY\""));

        service.execute(kinesisEvent);

        verify(aggregator, times(0)).aggregateTransaction(any());
    }

    //This test shows that if an exception is thrown while parsing json
    //it does not get propagated
    @Test
    void testBadJson() {

        when(kinesisRecord.getData()).thenReturn(getDummyData("INCORRECT_JSON"));

        service.execute(kinesisEvent);

        verify(aggregator, times(0)).aggregateTransaction(any());
    }

    @Test
    void testInsertEvent() {

        when(kinesisRecord.getData()).thenReturn(getDummyData("\"INSERT\""));

        service.execute(kinesisEvent);

        OriginTransactionData expected = new OriginTransactionData();
        expected.setUserId(2);
        expected.setMerchant("TEST_MERCHANT_2");
        expected.setYear(2021);
        expected.setMonth(12);
        expected.setDay(2);
        expected.setTransaction("McDonalds UK Wolverhampton");
        expected.setTransactionHash("test2");
        expected.setAmount(75.98502660464716);

        verify(aggregator, times(1)).aggregateTransaction(expected);
    }



    private ByteBuffer getDummyData(String eventName) {
        return ByteBuffer.wrap(getDummyEvent(eventName).getBytes(StandardCharsets.UTF_8));
    }

    private String getDummyEvent(String eventName) {
        return "{\n" +
                "    \"awsRegion\": \"eu-west-1\",\n" +
                "    \"eventID\": \"c69360a2-3407-4d63-838c-83401eab8d7e\",\n" +
                "    \"eventName\": " +
                eventName +
                ",\n" +
                "    \"userIdentity\": null,\n" +
                "    \"recordFormat\": \"application/json\",\n" +
                "    \"tableName\": \"user-transactions\",\n" +
                "    \"dynamodb\": {\n" +
                "        \"ApproximateCreationDateTime\": 1626345523501,\n" +
                "        \"Keys\": {\n" +
                "            \"transactionHash\": {\n" +
                "                \"S\": \"test2\"\n" +
                "            }\n" +
                "        },\n" +
                "        \"NewImage\": {\n" +
                "            \"userId\": {\n" +
                "                \"N\": \"2\"\n" +
                "            },\n" +
                "            \"month\": {\n" +
                "                \"N\": \"12\"\n" +
                "            },\n" +
                "            \"year\": {\n" +
                "                \"N\": \"2021\"\n" +
                "            },\n" +
                "            \"amount\": {\n" +
                "                \"N\": \"75.98502660464716\"\n" +
                "            },\n" +
                "            \"day\": {\n" +
                "                \"N\": \"2\"\n" +
                "            },\n" +
                "            \"merchant\": {\n" +
                "                \"S\": \"TEST_MERCHANT_2\"\n" +
                "            },\n" +
                "            \"transaction\": {\n" +
                "                \"S\": \"McDonalds UK Wolverhampton\"\n" +
                "            },\n" +
                "            \"transactionHash\": {\n" +
                "                \"S\": \"test2\"\n" +
                "            }\n" +
                "        },\n" +
                "        \"SizeBytes\": 142\n" +
                "    },\n" +
                "    \"eventSource\": \"aws:dynamodb\"\n" +
                "}\n" +
                "\n" +
                "\u001B[36m10:38:50.918\u001B[0;39m \u001B[1;30m[main]\u001B[0;39m \u001B[34mINFO \u001B[0;39m \u001B[35mc.e.l.NewUserTransactionConsumerService\u001B[0;39m - RAW DATA = {\"awsRegion\":\"eu-west-1\",\"eventID\":\"c69360a2-3407-4d63-838c-83401eab8d7e\",\"eventName\":\"REMOVE\",\"userIdentity\":null,\"recordFormat\":\"application/json\",\"tableName\":\"user-transactions\",\"dynamodb\":{\"ApproximateCreationDateTime\":1626345523501,\"Keys\":{\"transactionHash\":{\"S\":\"test2\"}},\"OldImage\":{\"userId\":{\"N\":\"2\"},\"month\":{\"N\":\"12\"},\"year\":{\"N\":\"2021\"},\"amount\":{\"N\":\"75.98502660464716\"},\"day\":{\"N\":\"2\"},\"merchant\":{\"S\":\"TEST_MERCHANT_2\"},\"transaction\":{\"S\":\"McDonalds UK Wolverhampton\"},\"transactionHash\":{\"S\":\"test2\"}},\"SizeBytes\":142},\"eventSource\":\"aws:dynamodb\"}";
    }
}